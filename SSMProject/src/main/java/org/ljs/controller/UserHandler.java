package org.ljs.controller;

import org.ljs.entity.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.ljs.service.UserService;

import java.util.List;

/**
 * Created by SunShine on 2020/3/31.
 */
@Controller
public class UserHandler {

    @Autowired
    private UserService userService;

    @GetMapping("/findAll")
    @ResponseBody
    public List<Users> findAll(){


        return userService.findAll();
    }


}
