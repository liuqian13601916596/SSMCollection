package org.ljs.repository;

import org.ljs.entity.Users;

import java.util.List;

/**
 * Created by SunShine on 2020/3/31.
 */
public interface UserRepository {

    public List<Users> findAll();

}
