package org.ljs.service;

import org.ljs.entity.Users;

import java.util.List;

/**
 * Created by SunShine on 2020/3/31.
 */
public interface UserService {

    public List<Users> findAll();
}
