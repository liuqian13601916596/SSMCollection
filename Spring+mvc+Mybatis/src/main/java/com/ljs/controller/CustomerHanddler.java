package com.ljs.controller;

import com.ljs.repository.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/test")
public class CustomerHanddler {
    @Autowired
    private CustomerService customerService;
    @RequestMapping("/ssm")
    public ModelAndView sel(){
        ModelAndView modelAndView =new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.addObject("list",customerService.selCustomer());
     return  modelAndView;
    }
}
