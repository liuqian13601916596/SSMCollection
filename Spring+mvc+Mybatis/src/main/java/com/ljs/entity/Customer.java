package com.ljs.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
/*Serializable设置二级缓存*/
public class Customer implements Serializable {
    private  String cid;
    private String cname;

}
