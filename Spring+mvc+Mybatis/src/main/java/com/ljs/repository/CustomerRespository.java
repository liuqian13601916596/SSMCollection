package com.ljs.repository;

import com.ljs.entity.Customer;

import java.util.List;

public interface CustomerRespository {
    public List<Customer> selCustomer();
}
