package com.ljs.repository;

import com.ljs.entity.Customer;

import java.util.List;

public interface CustomerService {
    public List<Customer> selCustomer();
}
