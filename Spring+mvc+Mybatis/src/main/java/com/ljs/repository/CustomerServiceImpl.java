package com.ljs.repository;

import com.ljs.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service/*接口注解*/
public class CustomerServiceImpl  implements CustomerService{
    @Autowired/*自动重写*/
    private CustomerRespository customerRespository;
    @Override
    public List<Customer> selCustomer() {
        return customerRespository.selCustomer();
    }
}
