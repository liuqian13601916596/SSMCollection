package org.ljs.entity;

import lombok.Data;

/**
 * Created by SunShine on 2020/3/31.
 */
@Data
public class User {
    private int id;
    private String name;

    private int age;
}
