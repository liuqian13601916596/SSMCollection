package org.ljs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.ljs.entity.User;
import org.ljs.repository.UserRepository;
import org.ljs.service.UserService;

import java.util.List;

/**
 * Created by SunShine on 2020/3/31.
 */
@Service
public class UserServiceImpl implements UserService{


    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> findAll() {

        return userRepository.findAll();
    }
}
