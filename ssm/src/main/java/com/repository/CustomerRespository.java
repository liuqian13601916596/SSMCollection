package com.repository;

import com.entity.Customer;


import java.util.List;

public interface CustomerRespository {
    public List<Customer> selCustomer();
}
