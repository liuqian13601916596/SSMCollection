package com.service.impl;

import com.entity.Customer;
import com.repository.CustomerRespository;
import com.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service/*接口注解*/
public class CustomerServiceImpl  implements CustomerService {
  @Autowired
  private CustomerRespository  customerRespository;
    @Override
    public List<Customer> selCustomer() {
/*       List<Customer> list=new ArrayList<>();
        Customer c1=new Customer(1,"ss");
        Customer c2=new Customer(1,"aa");
        Customer c3=new Customer(1,"bb");
        list.add(c1);
        list.add(c2);
        list.add(c3);*/
      List<Customer> list1=customerRespository.selCustomer();
        System.out.println("调用了方法"+list1);
        return list1;
    }
}
